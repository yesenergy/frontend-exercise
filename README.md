Yes Energy developer activity

## Installing / Running

1. Install Docker
2. Clone the repo into a local directory
3. In that directory run ```docker-compose up```

You will likely have to wait a good bit of time, as yarn can take quite a bit of time to install dependencies in the React project.  You should see a log message referencing localhost:3000, when you see that, everything is up and running.

This long wait should only happen the first time you run.
## Using the application

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.